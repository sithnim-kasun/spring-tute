/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Action;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kasun
 */
public class LoginAction extends ActionSupport {

    String userName, password;
    List testList;

    //##########################################################################
    //##########################################################################

    public List getTestList() {
        return testList;
    }

    public void setTestList(List testList) {
        this.testList = testList;
    }
    
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //##########################################################################
    @Override
    public String execute() {
        testList=new ArrayList();
        if (getUserName().equals("admin") && getPassword().equals("123456")) {
            
            testList.add("item 1");
            testList.add("item 2");
            testList.add("item 3");
            testList.add("item 4");
            testList.add("item 5");
            testList.add("item 6");

            return "success";
        } else {
            return "error";
        }
    }
}
