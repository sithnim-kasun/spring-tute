

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Login  Success!</h1>
        <h4>Welcome ${userName}</h4>
        <c:forEach var="itm" items="${testList}">
            <h4>${itm}</h4>
        </c:forEach>
        <a href="viewProducts" >View Products</a>
        <a href="viewWorkers" >View Workers</a>
    </body>
</html>
