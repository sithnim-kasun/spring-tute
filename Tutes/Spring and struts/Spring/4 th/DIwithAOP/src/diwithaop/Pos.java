package diwithaop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Pos {

    static DBController dBController;
    

    public Pos(DBController dBController) {
        Pos.dBController = dBController;
    }

    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("diwithaop/SpringXMLConfig.xml");
//        POSController pOSController = (POSController) ac.getBean("posControllerBean");
//        pOSController.save();
        dBController.save();
    }
}
