package diwithaop;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class BillAspect {

    BillController billController;

    public BillAspect(BillController billController) {
        this.billController = billController;
    }
    

    @After("execution(public void search())")
    public void print() {
        billController.print();
    }
    
}
