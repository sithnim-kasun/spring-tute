package diwithaop;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class DBAspect {

    DBController dBController;

    public DBAspect(DBController dBController) {
        this.dBController = dBController;
    }

    @After("execution(public void save())")
    public void forSearch() {
        dBController.search();
    }

    @Before("execution(public void search())")
    public void forReset() {
        dBController.reset();
    }
}
