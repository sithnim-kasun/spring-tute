package diwithaop;

public class POSController implements DBController, BillController {

    String searchAdvice, saveAdvice;

    @Override
    public void save() {
        System.out.println(saveAdvice);
    }

    @Override
    public void search() {
        System.out.println(searchAdvice);
    }

    @Override
    public void reset() {
        System.out.println("reset");
    }
    
    @Override
    public void print() {
        System.out.println("Printed.................");
    }

    /*
     ######################### GETTERS AND SETTER  ##############################
     */
    public String getSearchAdvice() {
        return searchAdvice;
    }

    public void setSearchAdvice(String searchAdvice) {
        this.searchAdvice = searchAdvice;
    }

    public String getSaveAdvice() {
        return saveAdvice;
    }

    public void setSaveAdvice(String saveAdvice) {
        this.saveAdvice = saveAdvice;
    }

    

}
