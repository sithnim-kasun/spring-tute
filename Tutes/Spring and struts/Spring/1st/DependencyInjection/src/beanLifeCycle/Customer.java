/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanLifeCycle;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author kasun
 */
public class Customer {
    
    public static void main(String[] args) {
       ApplicationContext context = new ClassPathXmlApplicationContext("beanLifeCycle/SpringConfig.xml");
       Restaurant ob=(Restaurant) context.getBean("RestaurantBean");
       ((AbstractApplicationContext)context).registerShutdownHook();
       ob.welcome();
       ob.prepareTea();
       ob.printWorkers();
    }
}
