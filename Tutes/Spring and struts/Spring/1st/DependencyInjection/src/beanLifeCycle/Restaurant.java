/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanLifeCycle;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 *
 * @author kasun
 */
public class Restaurant {

    

    /*
     constructor way
        public Restaurant(TeaMaker tm) {
        this.tm = tm;
     }
     */
    
    //setter way
    public void setTm(TeaMaker tm) {
        this.tm = tm;
    }

    public void welcome() {
        System.out.println(welcomeNote);
    }

    public void prepareTea() {
        tm.prepareTea();
    }
    
    public void printWorkers(){
        for (Object workwer : workwers) {
            System.out.println(workwer);
        }
    }
    
    @PostConstruct
    public void initRestaurant(){
        System.out.println("init restaurant.....");
    }
    
    @PreDestroy
    public void destroyRestaurant(){
        System.out.println("destroy restaurant.......");
    }

    String welcomeNote;
    TeaMaker tm;
    List workwers;

    /*
     ############################################################################
     */
    public void setWelcomeNote(String welcomeNote) {
        this.welcomeNote = welcomeNote;
    }

    public void setWorkwers(List workwers) {
        this.workwers = workwers;
    }
    

}
