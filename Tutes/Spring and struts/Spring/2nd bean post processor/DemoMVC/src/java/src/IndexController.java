/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 *
 * @author kasun
 */
public class IndexController extends AbstractController {

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("---------- " + request.getRequestURI());

        ModelAndView mav = null;

        String requestURI = request.getRequestURI();
        if (requestURI.contains("SubmitSuccess.htm")) {
            mav = new ModelAndView("viewindexdata");
            mav.addObject("msg", "Hello : " + request.getParameter("name"));
        } else if (requestURI.contains("index.htm")) {

            mav = new ModelAndView("index").addObject("msg", "ane pala pala");
        }
        return mav;
    }

}
