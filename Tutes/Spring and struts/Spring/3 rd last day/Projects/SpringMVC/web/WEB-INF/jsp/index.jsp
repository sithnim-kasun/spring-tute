<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <h1>${msg}</h1>
        <form action="submitSuccess.htm" method="post">
            <table>
                <tr>
                    <td>Name:</td>
                    <td><input id="name" name="name" type="text"/></td>
                </tr>
                <tr>
                    <td>Country:</td>
                    <td><input id="countrry" name="countrry" type="text"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" id="btn-submit" value="Submit"/></td>
                </tr>
            </table>
        </form>
    </body>
</html>
