/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kalanasarange.springMVC.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;


/**
 *
 * @author Kalana Sarange
 */
public class Controller extends AbstractController{

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("------------ " + request.getRequestURI());
        ModelAndView model = null;
        String requestURI = request.getRequestURI();
        
        if(requestURI.contains("submitsuccess.htm")){
            model = new ModelAndView("ViewData");
            model.addObject("msg", "Hello : " + request.getParameter("name"));
        }else{
            
        }
        model.addObject("msg", "Hello : " +request.getParameter("name"));
        return model;
    }
    
}
