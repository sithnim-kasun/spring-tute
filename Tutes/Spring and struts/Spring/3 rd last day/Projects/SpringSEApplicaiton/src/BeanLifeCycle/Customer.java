/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BeanLifeCycle;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Kalana Sarange
 */
public class Customer {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("BeanLifeCycle/SpringConfig.xml");
        Restaurant restaurantBean = (Restaurant) applicationContext.getBean("RestaurantBean");
        ((AbstractApplicationContext)applicationContext).registerShutdownHook();
        restaurantBean.welcome();
        restaurantBean.prepareTea();
        restaurantBean.readWorkers();
    }
}
