/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BeanLifeCycle;

import java.util.List;

/**
 *
 * @author Kalana Sarange
 */
public class Restaurant {
    
    private String welcomeNote;
    private TeaMaker teaMaker;
    private List workers;
    
//    Use this way for construter method
//    public Restaurant(TeaMaker teaMaker) {
//        this.teaMaker = teaMaker;
//    }
    
//    Use this way for Setter methohd
    public void setTeaMaker(TeaMaker teaMaker) {
        this.teaMaker = teaMaker;
    }
        
    public void welcome(){
        System.out.println(getWelcomeNote());
    }

    public void prepareTea(){
        teaMaker.prepareTea();
    }
    
    public void readWorkers(){
        for (Object worker : workers) {
            System.out.println(worker);
        }
    }
    
    public void init(){
        System.out.println("init called");
    }
    
    public void destroy(){
        System.out.println("destroy called");
    }
    
//    Getter & Setter
    public String getWelcomeNote() {
        return welcomeNote;
    }

    public void setWelcomeNote(String welcomeNote) {
        this.welcomeNote = welcomeNote;
    }

    public void setWorkers(List workers) {
        this.workers = workers;
    }
    
}
