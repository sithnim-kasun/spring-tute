/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DependancyInjectionTesting;

/**
 *
 * @author Kalana Sarange
 */
public class Tea implements TeaMaker{

    @Override
    public void prepareTea() {
        System.out.println("Prepare tea called");
    }

}
