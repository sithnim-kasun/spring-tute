/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DependancyInjectionTesting;

/**
 *
 * @author Kalana Sarange
 */
public interface TeaMaker {
    public void prepareTea();
}
