/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DependancyInjectionTesting;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;

/**
 *
 * @author Kalana Sarange
 */
public class DemoBeanPostProcessor2 implements BeanPostProcessor, Ordered{

    @Override
    public Object postProcessBeforeInitialization(Object o, String string) throws BeansException {
        System.out.println("Before init... boo 2 - " + string);
        return o;
    }

    @Override
    public Object postProcessAfterInitialization(Object o, String string) throws BeansException {
        System.out.println("After init... boo 2 - " + string);
        return o;
    }

    @Override
    public int getOrder() {
        return 0;
    }
    
}
