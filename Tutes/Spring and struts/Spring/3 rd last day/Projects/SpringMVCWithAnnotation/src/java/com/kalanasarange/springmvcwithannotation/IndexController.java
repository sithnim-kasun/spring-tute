/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kalanasarange.springmvcwithannotation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

    @RequestMapping(value="/index.htm")
    public ModelAndView indexAction() {
        ModelAndView model = new ModelAndView("index");
        return model;
    }
    
//    @RequestMapping(value="/indexSubmit.htm")
//    public ModelAndView indexSubmit(@RequestParam("name") String name,@RequestParam("hobby") String hobby) {
//        ModelAndView model = new ModelAndView("viewdata");
//        model.addObject("name",name);
//        model.addObject("hobby",hobby);
//        return model;
//    }
    @RequestMapping(value="/indexSubmit.htm")
    public ModelAndView indexSubmit(@ModelAttribute("index") IndexDataHolder dataHolder) {
        ModelAndView model = new ModelAndView("viewdata");
        return model;
    }
}
